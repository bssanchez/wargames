<?php

//FUNCIONES para mostrar mensajes
//Mensaje de error
function showMenErr($titulo, $descripcion, $numero = "", $enlace = "") {
    //$sty = '<style>a{color:#810808;text-decoration:none;background-color:#ffffff;font-weight:bold;}a:hover{text-decoration:underline;}</style>';
    $mensaje = "<html><head><title>Confirmacion - WargamesHxC</title></head><body bgcolor='#000'>";
    $mensaje.="<table style='position: relative; width:400px; margin-top: 40px; margin-bottom: 40px; left: 50%; margin-left:-200px; border: 1px #810808 solid;  text-align: center; padding:1px;'>";
    $mensaje.="<tr style='background-color:#810808;'><td>";
    $mensaje.="<font color='white' size='2' face='Arial'><strong>Error " . $numero . "</strong>: $titulo";
    $mensaje.="</td></tr>";
    $mensaje.="<tr><td>";
    $mensaje.="<font color='#CCCCCC' size='3' face='Arial'>$descripcion";
    if ($enlace != "")
        $mensaje.="<div align='center'>$enlace</div>";
    $mensaje.="</td></tr>\n";
    $mensaje.="</table></body></html>";
    return $mensaje;
}

//Mensaje normal
function showMen($titulo, $descripcion, $enlace = "") {
    //$sty = '<style>a{color:#0000FF;text-decoration:none;background-color:#ffffff;font-weight:bold;}a:hover{text-decoration:underline;}</style>';
    $mensaje = "<html><head><title>Confirmacion - WargamesHxC</title></head><body bgcolor='#000'>";
    $mensaje.="<table style='position: relative; width:400px; top: 40px; left: 50%; margin-left:-200px; border: 1px #810808 solid;  text-align: justify; padding:1px;'>";
    $mensaje.="<tr style='background-color:#810808;'><td>";
    $mensaje.="<font color='white' size='2' face='Arial'><strong>Informe:</strong> $titulo";
    $mensaje.="</td></tr>";
    $mensaje.="<tr><td>";
    $mensaje.="<font color='#CCCCCC' size='3' face='Arial'>$descripcion";
    if ($enlace != "")
        $mensaje.="<div style='text-align: center; margin-top: 10px;'>$enlace</div>";
    $mensaje.="</td></tr>";
    $mensaje.="</table></body></html>";
    return $mensaje;
}

?>
