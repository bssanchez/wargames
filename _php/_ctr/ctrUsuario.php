<?php

include("../showMens.php");
include('../_cls/clsUsuario.php');
$objUsu = new clsUsuario();
//Captura de variables xDDDD	
if (isset($_GET["mth"])) {
    if ($_GET["mth"] == "R") {
        $nck = $_POST["txtNickR"];
        $pwd = $_POST["txtPwD"];
        $nom = $_POST["txtNickR"];
        $crr = $_POST["txtCorr"];
        $rsc = $_POST["txtRSec"];
        $psc = $_POST["selPSec"];

        if ((isset($nck) && $nck != "") && (isset($pwd) && $pwd != "") && (isset($nom) && $nom != "") && (isset($crr) && $crr != "") && (isset($rsc) && $rsc != "") && (isset($psc) && $psc != "")) {
            $objUsu->setUsu($nck);
            $objUsu->setNom($nom);
            $objUsu->setCorr($crr);

            $xD = $objUsu->cmpUsu();
            if ($xD == "NO") {
                $des = "El usuario con los datos que has ingresado ya existe, asi que puedes iniciar session...";
                echo showMenErr("Usuario Ya Existe", $des, "402", "<a href='../../1nd3x.php'>Redireccioname Ya!</a>");
            } else {
                $objUsu->setUsu($nck);
                $objUsu->setPass($pwd);
                $objUsu->setNom($nom);
                $objUsu->setCorr($crr);
                $objUsu->setResSec($rsc);
                $objUsu->setPreSec($psc);

                $objUsu->GuardarUsu();
                $des = "Muy bien, ahora eres usuario de la web, puedes iniciar session.";
                echo showMen("Registro Creado", $des, "<a href='../../1nd3x.php'>Redireccioname Ya!</a>");
            }
        } else {
            $des = "Uno o mas campos estan vacios, por favor llenalos he intenta nuevamente.";
            echo showMenErr("Registro Creado", $des, "402", "<a href='javascript: history.back(1);'>Redireccioname Ya!</a>");
        }
    } else if ($_GET["mth"] == "L") {
        $objUsu->setUsu($_POST["txtNickL"]);
        $objUsu->setPass($_POST["txtPwd"]);
        $objUsu->iniSesion();
        $_ushxc = $objUsu->getUsu();
        $_imhxc = $objUsu->getImg();
        if ($_ushxc != null) {
            session_start();
            $_SESSION["_userHxC"] = $_ushxc;
            $_SESSION["_imgHxC"] = $_imhxc;
            $des = "Definitivamente eres un usuario... Bienvenido a los Wargames de HackxCrack.";
            echo showMen("Acceso Permitido", $des, "<a style='color:#CCCCFF; text-decoration:none;' href='../../1nd3x.php'>Redireccioname Ya!</a>");
        } else {
            $des = "Usuario no autorizado, acceso denegado... xDDD";
            echo showMenErr("Acceso Denegado", $des, "403", "<a style='color:#CCCCFF; text-decoration:none;' href='javascript:history.back(1);'>Salir Ya!</a>");
        }
    } else if ($_GET["mth"] == "I") {
        $objUsu->setImg($_POST["txtNImG"]);
        $objUsu->setNom($_POST["hidUsu"]);
        $objUsu->setPass($_POST["txtPwD"]);
        $objUsu->chgImagen();
        $des = "Tu imagen de perfil se ha actualizado correctamente...";
        echo showMen("Cambio de Imagen de Perfil", $des, "<a style='color:#CCCCFF; text-decoration:none;' href='javascript:window.close();'>Salir Ya!</a>");
    } else if ($_GET["mth"] == "C") {
        $objUsu->setNom($_POST["hidUsu"]);
        $objUsu->setPass($_POST["txtAPwD"]);
        $objUsu->setNPass($_POST["txtNPwDV"]);
        $objUsu->chgPassword();
        $des = "Tu Contrase&ntilde;a se ha actualizado correctamente...";
        echo showMen("Cambio de Contrase&ntilde;a", $des, "<a style='color:#CCCCFF; text-decoration:none;' href='javascript:window.close();'>Salir Ya!</a>");
    } else if ($_GET["mth"] == "RC") {
        $objUsu->setNPass($_POST["txtPwDV"]);
        $objUsu->setID($_POST["hddID"]);
        $objUsu->recPassword();
        $des = "Tu contrase&ntilde;a ha sido actualizada exitosamente...";
        echo showMen("Recuperacion de Contrase&ntilde;a", $des, "<a style='color:#CCCCFF; text-decoration:none;' href='javascript:window.close();'>Salir Ya!</a>");
    } else {
        $des = "No se ha asignado ningun metodo valido... o estas intentando cosas extra&ntilde;as... xD";
        echo showMenErr("Acceso Denegado", $des, "403", "<a style='color:#CCCCFF; text-decoration:none;' href='javascript:history.back(1);'>Salir Ya!</a>");
    }
} else {
    $des = "No se ha asignado ningun metodo... o estas intentando cosas extra&ntilde;as... xD";
    echo showMenErr("Acceso Denegado", $des, "403", "<a style='color:#CCCCFF; text-decoration:none;' href='javascript:history.back(1);'>Salir Ya!</a>");
}