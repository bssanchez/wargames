<?php

class clsUsuario {

    public $conn; //conexion
    private $nUsu; //nick de usuario
    private $mUsu; //Nombre del usuario
    private $cUsu; //correo de usuario
    private $pUsu; //password usuario
    private $iUsu; //imagen de usuario
    private $qUsu; //pregunta de ususario
    private $rUsu; //respuesta de usuario
    private $npUsu;  //nueva password de usuario
    private $idUsu; //id de usuario

    //metodo constructor para la clase conexion

    public function __construct() {
        include("clsConex.php");
        $this->conn = $conex;
    }

    //Asignacion de valores a las variables
    public function setUsu($val) {
        $this->nUsu = stripslashes($val);
        $this->nUsu = htmlentities($this->nUsu);
        $this->nUsu = strtolower($this->nUsu);
        $this->nUsu = strip_tags($this->nUsu);
        $this->nUsu = mysql_real_escape_string($this->nUsu);
    }

    public function setNom($val) {
        $this->mUsu = stripslashes($val);
        $this->mUsu = htmlentities($this->mUsu);
        $this->mUsu = strip_tags($this->mUsu);
        $this->mUsu = mysql_real_escape_string($this->mUsu);
    }

    public function setPass($val) {
        $this->pUsu = stripslashes($val);
        $this->pUsu = sha1(md5($this->pUsu));
    }

    public function setCorr($val) {
        $this->cUsu = stripslashes($val);
        $this->cUsu = htmlentities($this->cUsu);
        $this->cUsu = strip_tags($this->cUsu);
        $this->cUsu = mysql_real_escape_string($this->cUsu);
    }

    public function setImg($val) {
        $this->iUsu = stripslashes($val);
        $this->iUsu = mysql_real_escape_string($this->iUsu);
    }

    public function setID($val) {
        $this->idUsu = stripslashes($val);
        $this->idUsu = mysql_real_escape_string($this->idUsu);
    }

    public function setNPass($val) {
        $this->npUsu = stripslashes($val);
        $this->npUsu = sha1(md5($this->npUsu));
    }

    public function setPreSec($val) {
        $this->qUsu = stripslashes($val);
        $this->qUsu = mysql_real_escape_string($this->qUsu);
    }

    public function setResSec($val) {
        $this->rUsu = stripslashes($val);
        $this->rUsu = sha1(md5($this->rUsu));
    }

    //Retorno de variables
    public function getUsu() {
        return $this->nUsu;
    }

    public function getImg() {
        return $this->iUsu;
    }

    //Metodos xD
    public function GuardarUsu() {
        $qSql = "insert into tb_userhxc (_nickUsu, _nomUsu, _corUsu, _pasUsu, _imgUsu, _preSeg, _resSeg) values ('$this->nUsu','$this->mUsu','$this->cUsu','$this->pUsu', './_img/perf.gif', '$this->qUsu', '$this->rUsu');";
        $eSql = mysql_query($qSql);
        if (mysql_error()) {
            echo "<script>alert('Error, vuelve a intentarlo...');history.back(1);</script>";
        }
    }

    public function iniSesion() {
        $qSql = "select * from tb_userhxc where _nickUsu = '$this->nUsu' and _pasUsu = '$this->pUsu'";
        $eSql = mysql_query($qSql);
        if (@mysql_num_rows($eSql) == 1) {
            $dat = mysql_fetch_array($eSql);
        }
        $this->nUsu = isset($dat) ? $dat["_nomUsu"] : "";
        $this->iUsu = isset($dat) ? $dat["_imgUsu"] : "";
    }

    public function passWord($tama, $mayu, $nume, $char) {
        $src = 'abcdefghijklmnopqrstuvwxyz';
        if ($mayu == 1) {
            $src .= 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        }
        if ($nume == 1) {
            $src .= '0123456789';
        }
        if ($char == 1) {
            $src .= '#%=*+-_';
        }

        if ($tama > 0) {
            $pwd = "";
            $src = str_split($src, 1);

            for ($i = 1; $i <= $tama; $i++) {
                mt_srand((double) microtime() * 1000000);
                $num = mt_rand(1, count($src));
                $pwd .= $src[$num - 1];
            }
        }
        $this->pUsu = $pwd;
    }

    public function chgImagen() {
        $qSql = "update tb_userhxc set _imgUsu = '$this->iUsu' where _nomUsu = '$this->mUsu' and _pasUsu = '$this->pUsu'";
        $eSql = mysql_query($qSql);
        if (mysql_error() || mysql_affected_rows() == 0) {
            echo "<script>alert('Error, vuelve a intentarlo... (Tal vez la contrasena no es correcta)');history.back(1);</script>";
        }
    }

    public function chgPassword() {
        $qSql = "update tb_userhxc set _pasUsu = '$this->npUsu' where _nomUsu = '$this->mUsu' and _pasUsu = '$this->pUsu'";
        $eSql = mysql_query($qSql);
        if (mysql_error() || mysql_affected_rows() == 0) {
            echo "<script>alert('Error, vuelve a intentarlo... (Tal vez la ant. contrasena no es correcta)');history.back(1);</script>";
        }
    }

    public function recPassword() {
        $qSql = "update tb_userhxc set _pasUsu = '$this->npUsu' where _idUsu = '$this->idUsu'";
        $eSql = mysql_query($qSql);
        if (mysql_error() || mysql_affected_rows() == 0) {
            echo "<script>alert('Error, vuelve a intentarlo...');history.back(1);</script>";
        }
    }

    public function cmpUsu() {
        $qSql = "select * from tb_userhxc where _nickUsu = '$this->nUsu' or _corUsu = '$this->cUsu' or _nomUsu = '$this->mUsu'";
        $eSql = mysql_query($qSql);
        $cSql = mysql_num_rows($eSql);
        if ($cSql > 0) {
            return "NO";
        } else {
            return "SI";
        }
    }

}