<?php
session_start();
if (isset($_SESSION["_userHxC"]) && $_SESSION["_userHxC"] != "") {
    $user = $_SESSION["_userHxC"];
    $img = $_SESSION["_imgHxC"];
    setcookie("permitir", "NO", time() + 3600);
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>Wargames</title>
        <!-- Meta Tags -->
        <meta http-equiv="Content-Type" content="text/html;charset=utf-8"/>
        <meta http-equiv="Content-Style-Type" content="text/css"/>
        <!-- /Meta Tags -->

        <!-- Estilos -->
        <link rel="shortcut icon" href="favicon.png" type="image/png"/>
        <link rel="stylesheet" href="./_css/style.css" type="text/css" media="screen"/>
        <!-- /Estilos -->

        <!-- Scripts -->
        <script type="text/javascript" src="./_js/jquery-1.7.min.js"></script>
        <script type="text/javascript" src="./_js/ajax.js"></script>
        <script type="text/javascript" src="./_js/_js.js"></script>
        <!-- /Scripts -->
    </head>
    <body onload="webPag('./_wpag/inicio.htm');">

        <div id="container">
            <div id="body_image"></div>
            <!-- Start of Page Header -->
            <div id="page_header">
                <h1><span>WargamesHxC</span></h1>
                <div class="clearthis">&nbsp;</div>
            </div>

            <!-- Menu -->
            <div id="page_menu">
                <ul>
                    <li class="online"><a href="javascript: webPag('./_wpag/inicio.htm');" title="Inicio"><span>INICIO</span></a><span></span></li>
                    <li class="downloads"><a href="javascript: webPag('./_wpag/retW.php');" title="Retos"><span>RETOS</span></a><span></span></li>
                    <li class="community"><a href="javascript: webPag('./_wpag/perfil.php');" title="Perfil"><span>PERFIL</span></a><span></span></li>
                    <li class="about"><a href="javascript: webPag('./_wpag/hof.php');" title="Los Mejores"><span>HALL OF FAME</span></a><span></span></li>
                </ul>
            </div>
            <!-- /Menu -->
            <div id="page_forms">

                <!-- Login -->
                <div id="userlogin">
                    <div id="userlogin_header">
                        <?php if (!isset($user) || $user == "") { ?><h2><span>Ingresar</span></h2><?php } else {
                            echo "<h3>" . $user . "</h3>";
                        } ?>
                    </div>
                    <?php
                    if (!isset($user) || $user == "") {
                        ?>
                        <form action="./_php/_ctr/ctrUsuario.php?mth=L" method="post">
                            <div id="field_username"> 
                                <strong><span>Usuario:</span></strong> <input type="text" name="txtNickL"/>
                            </div>
                            <div id="field_password"> 
                                <strong><span>Contrase&ntilde;a:</span></strong><input type="password" name="txtPwd"/>
                            </div>
                            <div id="button_enter">
                                <input src="_img/userlogin_enter.gif" alt="Enter" class="button" type="image"/> 
                            </div>
                        </form>
                        <div id="userlogin_links"> 
                            <a href="./_wpag/regUsu.php" id="register"><strong><span>Registrarse	&gt;&gt;</span></strong></a><br/>
                            <a href="javascript:showEmer('./_wpag/recPwd.php', '420','200');" title="De malas" id="notregister"><strong><span>No puedes entrar?</span></strong></a> 
                        </div>
                        <?php
                    } else {
                        ?>
                        <div style="text-align: center; width: 100%; height: 100%;">
                            <img src="<?php echo $img; ?>" alt="<?php echo $user; ?>" title="<?php echo $user; ?>" style="width: 170px; height: 192px; border: 1px #000 inset;"/>
                        </div>
    <?php
}
?>
                </div>
                <!-- /Login -->

                <!-- Busqueda -->
                <div id="sitesearch_header">
                    <h2><span>Buscar</span></h2>
                </div>
                <div id="sitesearch">
                    <form action="#" method="get" onsubmit="webPag('./_wpag/busq.php?s=' + this.s.value);
                return false;">
                        <div><input type="text" name="s"/>
                            <input src="_img/sitesearch_button.gif" alt="Go" class="button" type="image"/>
                        </div>
                        <div class="clearthis">&nbsp;</div>
                    </form>
                </div>
                <!-- /Busqueda -->
            </div>

            <div id="content_body">

            </div>

            <div id="clearthis_contentbody">&nbsp;</div>
        </div>
        <!-- Pie de pagina -->
        <div id="page_footer">
            Copyright &copy; <a href="http://www.tusitio.es" onclick="this.target = '_blank'">www.tusitio.es</a>
            | Developed by <a href="https://twitter.com/_kid_goth" onclick="this.target = '_blank'">kid_goth</a>
            <br/><img src="./_img/hacker.png" alt="Hacker" style="margin-top: 5px;"/>
            <a href="http://validator.w3.org/check?uri=referer" onclick="this.target = '_blank'">
                <img src="./_img/xhtml.png" alt="Valid XHTML 1.0 Strict" title="Valid XHTML 1.0 Strict" style="height: 15px; width: 80px;" />
            </a>
            <br/>2012
        </div>
        <div id="cnt_visit">
<?php include('./_php/cont.php'); ?>
        </div>
            <?php if (isset($user) || $user != "") { ?>
            <div id="salir" onclick="window.location = '_php/_salir.php'" title="Cerrar Sesion">
            </div>
        <?php } ?>
        <!-- / Pie de pagina -->
    </body>
</html>
