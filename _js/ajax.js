// JavaScript Document

//-->CODIGO  AJAX-----------------------------------------------------------------------------------------------------
function nuevoAjax() {
    var xmlhttp = false;
    try
    {
        xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
    }
    catch (e)
    {
        try
        {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        catch (E)
        {
            xmlhttp = false;
        }
    }

    if (!xmlhttp && typeof XMLHttpRequest != 'undefined')
    {
        xmlhttp = new XMLHttpRequest();
    }
    return xmlhttp;
}

function webPag(pag)
{
    var contenedor;
    var page = pag.replace('../', '');
    contenedor = document.getElementById('content_body');
    contenedor.innerHTML = '<div style="width: 774px;"><img src="./_img/loading.gif" alt="Cargando..." style="position: relative; top: 50%; left: 50%; margin-left: -200px;"></div>';
    ajax = nuevoAjax();
    ajax.open("GET", page, true);
    ajax.onreadystatechange = function() {
        if (ajax.readyState == 4)
        {
            contenedor.innerHTML = ajax.responseText;
        }
    }
    ajax.send(null)
}


//----------------------------------------------Validar Nick--------------------------------------//

function obtDatosU(pag, id)
{
    var contenedor;
    contenedor = document.getElementById(id);
    ajax = nuevoAjax();
    ajax.open("GET", pag);
    ajax.onreadystatechange = function()
    {
        if (ajax.readyState == 4)
        {
            var rt = ajax.responseText.substr(0, 2);
            if (rt == "NO")
            {
                contenedor.innerHTML = "<img src='../_img/err.png' width='16px' height='16px' title='Ya existe' /><input type='hidden' value='NO' id='hdvalU'>";
            }
            else
            {
                contenedor.innerHTML = "<img src='../_img/acc.png' width='16px' height='16px' /><input type='hidden' value='SI'  id='hdvalU'>";
            }
        }
    }
    ajax.send(null);
}

function obtDatosC(pag, id)
{
    var contenedor;
    contenedor = document.getElementById(id);
    ajax = nuevoAjax();
    ajax.open("GET", pag);
    ajax.onreadystatechange = function()
    {
        if (ajax.readyState == 4)
        {
            var rt = ajax.responseText.substr(0, 2);
            if (rt == "NO")
            {
                contenedor.innerHTML = "<img src='../_img/err.png' width='16px' height='16px' title='Ya existe' /><input type='hidden' value='NO' id='hdvalC' />";
            }
            else
            {
                contenedor.innerHTML = "<img src='../_img/acc.png' width='16px' height='16px' /><input type='hidden' value='SI'  id='hdvalC' />";
            }
        }
    }
    ajax.send(null);
}

function cmpUser(tcl, id, mth)
{
    if (tcl == "")
    {
        document.getElementById(id).style.display = "none";
        document.getElementById('txtNickR').style.width = "180px";
    }
    else
    {
        document.getElementById(id).style.display = "";
        document.getElementById('txtNickR').style.width = "164px";
        obtDatosU("../_php/_ctr/ctrCmp.php?mth=U&cmp=" + tcl, id);
    }
}


function cmpCorr(tcl, id, mth)
{
    if (tcl == "")
    {
        document.getElementById(id).style.display = "none";
        document.getElementById('txtCorr').style.width = "180px";
    }
    else
    {
        document.getElementById(id).style.display = "";
        document.getElementById('txtCorr').style.width = "164px";
        obtDatosC("../_php/_ctr/ctrCmp.php?mth=C&cmp=" + tcl, id);
    }
}

//---------------------------------------------------------------------------------------------
function ValidarNum(evt)
{
    if (evt.keyCode < 48 || evt.keyCode > 57)
        evt.returnValue = false;
}
//****************************************************************************************

