/*Texto javascript by KidGoth*/
$(document).ready(function()
{
    $("#frmReg").submit(function()
    {
        if ($("#txtNickR").val() == "" || $("#hdvalU").val() == "NO")
        {
            alert("El nick no es valido o ya esta en uso...");
            $("#txtNickR").val("");
            $("#txtNickR").focus();
            return false;
        }
        else if ($("#txtCorr").val() == "" || valEma($("#txtCorr").val()) == false || $("#hdvalC").val() == "NO")
        {
            alert("El correo no es valido o ya existe...");
            $("#txtCorr").focus();
            return false;
        }
        else
        {
            return true;
        }
    });
});

function valEma(email)
{
    var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
    return filter.test(email);
}

function showEmer(pagina, tamX, tamY)
{
    var opciones = "toolbar=no, location=no, directories=yes, status=no, menubar=no, scrollbars=yes, resizable=no, width=" + tamX + ", height=" + tamY + ", top=85, left=140";
    window.open(pagina, "", opciones);
}