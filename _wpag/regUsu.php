<html>
    <head>
        <script src="../_js/jquery-1.7.min.js" type="text/javascript"></script>
        <script type="text/javascript" src="../_js/ajax.js"></script>
        <script type="text/javascript" src="../_js/_js.js"></script>
        <script>
            $(document).ready(function()
            {
                $("#frmReg").submit(function()
                {
                    if ($("#txtPwD").val() != $("#txtPwDV").val())
                    {
                        alert("Las contrase" + String.fromCharCode(241) + "as no coinciden...");
                        $("#txtPwD").val("");
                        $("#txtPwDV").val("")
                        $("#txtPwD").focus();
                        return false;
                    }
                    if ($("#txtNickR").val() == "" || $("#txtCorr").val() == "" || $("#txtRSec").val() == "" || $("#txtPwD").val() == "" || $("#txtPwDV").val() == "")
                    {
                        alert("Alguno de los campos esta vacio,\npor favor verifiquelos e intente nuevamente");
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                });
            });
        </script>
        <style>
            .field { border: 1px #1A1A1A solid; margin-right: 5px; margin-top: 4px; width: 180px; height: 20px; }
            .field:focus { border: 1px #BB0000 solid; color: #BB0000 }
            .but { border: 1px #BB0000 solid; background: #000000; margin-right: 5px; margin-top: 4px; width: 100px; color: #FFF; height: 32px; }
            .but:focus { border: 1px #000000 dotted; background: #BB0000; }
            .cen { text-align:right; }
            table { border: 1px #000 dotted; padding: 15px; }
            #tit { background-color: #BB0000; color: #FFF; text-align: center; border: 2px #000000 dotted; font-size: 18px; }
            td { border: 1px #FFFFFF dotted; padding: 5px; }
        </style>
        <title>Wargame - HackXCrack (Registr de Usuario)</title>
        <link rel="stylesheet" href="../_styles/style.css"  type="text/css" media="screen" />
    </head>
    <body>
        <br />
        <form id="frmReg" method="post" action="../_php/_ctr/ctrUsuario.php?mth=R" autocomplete="off">
            <table width="500px" align="center">
                <tr>
                    <td width="100%" colspan="2" height="43" id="tit">Registro de Usuarios</td>
                </tr>
                <tr>
                    <td width="49%" height="43">Usuario: </td>
                    <td width="51%" class="cen"><input name="txtNickR" id="txtNickR" type="text" class="field" onkeyup ="cmpUser(this.value, 'cmpUsu', 'U');"><span id="cmpUsu"></span></td>
                </tr>
                <tr>
                <tr>
                    <td width="49%" height="43">Correo: </td>
                    <td width="51%" class="cen"><input name="txtCorr" id="txtCorr" type="text" class="field" onkeyup ="cmpCorr(this.value, 'cmpCorr', 'C');"><span id="cmpCorr"></span></td>
                </tr>
                <tr>
                <tr>
                    <td width="49%" height="43" >Pregunta Secreta: </td>
                    <td width="51%" class="cen">
                        <select name="selPSec" id="selPSec" class="field">
                            <option value="0">Sin pregunta, solo frase secreta</option>
                            <option value="1">Nombre de tu primera mascota?</option>
                            <option value="2">Primer apellido de tu (ex)novia?</option>
                            <option value="3">A&ntilde;o de Nacimiento de tu hermano(a) o primo?</option>
                            <option value="4">Nombre de profesor favorito?</option>
                            <option value="5">Numero de celular mas importante para ud?</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td width="49%" height="43">Respuesta Secreta: </td>
                    <td width="51%" class="cen"><input name="txtRSec" id="txtRSec" type="password" class="field"></td>
                </tr>
                <tr>
                    <td height="37">Contrase&ntilde;a: </td>
                    <td class="cen"><input name="txtPwD" id="txtPwD" type="password" class="field"></td>
                </tr>
                <tr>
                    <td height="47">Verifique Contrase&ntilde;a: </td>
                    <td class="cen"><input name="txtPwDV" id="txtPwDV" type="password" class="field"></td>
                </tr>
                <tr>
                    <td height="47" colspan="2" align="center"><label>
                            <input name="Submit" type="submit" class="but" value="Registrarse">
                        </label></td>
                </tr>
            </table>
        </form>
    </body>
</html>