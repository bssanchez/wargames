<?php
session_start();
if (isset($_SESSION["_userHxC"]) && $_SESSION["_userHxC"] != "") {
    $user = $_SESSION["_userHxC"];
}
include("../_php/_cls/clsConex.php");
include("../_php/showMens.php");
?>
<link href="./_styles/styRetW.css" rel="stylesheet" type="text/css">
<div align="center">
    <table>
        <tr>
            <td colspan="4" class="titP">SQL Injection y Blind SQL </td>
        </tr>
        <tr>
            <td class="tdtit titS">Titulo</td>
            <td class="tddes titS">Descripcion</td>
            <td class="tdlink titS">Idioma</td>
            <td class="tdapr titS">Link</td>
        </tr>
        <?php
        $qSql = "select * from tb_tuthxc where _tipTut = 'sql'";
        $eSql = mysql_query($qSql);
        if (mysql_error()) {
            echo showMenErr("Error en el servidor", "En estos momentos se ha producido un error en el servidor, por favor recarga la pagina y si aun no se soluciona contacta con el administrador...", "500", "<a href='javascript:document.reload();'>Recargar web</a>");
        } else {
            while ($dat = mysql_fetch_array($eSql)) {
                ?>
                <tr>
                    <td><?php echo $dat["_titTut"]; ?></td>
                    <td><?php echo $dat["_desTut"]; ?></td>
                    <td><?php echo $dat["_idiTut"]; ?></td>
                    <td><?php echo isset($user) ? "<a href='" . $dat["_urlTut"] . "' target='_blank'>Ir</a>" : "Registrate o Entra"; ?></td>
                </tr>
        <?php
    }
}
?>
    </table>
    <table>
        <tr>
            <td colspan="4" class="titP">RFI - LFI - XSS </td>
        </tr>
        <tr>
            <td class="tdtit titS">Titulo</td>
            <td class="tddes titS">Descripcion</td>
            <td class="tdlink titS">Idioma</td>
            <td class="tdapr titS">Link</td>
        </tr>
<?php
$qSql = "select * from tb_tuthxc where _tipTut = 'rlx'";
$eSql = mysql_query($qSql);
if (mysql_error()) {
    echo showMenErr("Error en el servidor", "En estos momentos se ha producido un error en el servidor, por favor recarga la pagina y si aun no se soluciona contacta con el administrador...", "500", "<a href='javascript:document.reload();'>Recargar web</a>");
} else {
    while ($dat = mysql_fetch_array($eSql)) {
        ?>
                <tr>
                    <td><?php echo $dat["_titTut"]; ?></td>
                    <td><?php echo $dat["_desTut"]; ?></td>
                    <td><?php echo $dat["_idiTut"]; ?></td>
                    <td><?php echo isset($user) ? "<a href='" . $dat["_urlTut"] . "' target='_blank'>Ir</a>" : "Registrate o Entra"; ?></td>
                </tr>
                <?php
            }
        }
        ?>
    </table>
    <table>
        <tr>
            <td colspan="4" class="titP">CriptoGrafia y Estageganografia</td>
        </tr>
        <tr>
            <td class="tdtit titS">Titulo</td>
            <td class="tddes titS">Descripcion</td>
            <td class="tdlink titS">Idioma</td>
            <td class="tdapr titS">Link</td>
        </tr>
<?php
$qSql = "select * from tb_tuthxc where _tipTut = 'cri'";
$eSql = mysql_query($qSql);
if (mysql_error()) {
    echo showMenErr("Error en el servidor", "En estos momentos se ha producido un error en el servidor, por favor recarga la pagina y si aun no se soluciona contacta con el administrador...", "500", "<a href='javascript:document.reload();'>Recargar web</a>");
} else {
    while ($dat = mysql_fetch_array($eSql)) {
        ?>
                <tr>
                    <td><?php echo $dat["_titTut"]; ?></td>
                    <td><?php echo $dat["_desTut"]; ?></td>
                    <td><?php echo $dat["_idiTut"]; ?></td>
                    <td><?php echo isset($user) ? "<a href='" . $dat["_urlTut"] . "' target='_blank'>Ir</a>" : "Registrate o Entra"; ?></td>
                </tr>
                <?php
            }
        }
        ?>
    </table>
    <table>
        <tr>
            <td colspan="4" class="titP">Ingenieria Inversa (BF)</td>
        </tr>
        <tr>
            <td class="tdtit titS">Titulo</td>
            <td class="tddes titS">Descripcion</td>
            <td class="tdlink titS">Idioma</td>
            <td class="tdapr titS">Link</td>
        </tr>
        <?php
        $qSql = "select * from tb_tuthxc where _tipTut = 'iiv'";
        $eSql = mysql_query($qSql);
        if (mysql_error()) {
            echo showMenErr("Error en el servidor", "En estos momentos se ha producido un error en el servidor, por favor recarga la pagina y si aun no se soluciona contacta con el administrador...", "500", "<a href='javascript:document.reload();'>Recargar web</a>");
        } else {
            while ($dat = mysql_fetch_array($eSql)) {
                ?>
                <tr>
                    <td><?php echo $dat["_titTut"]; ?></td>
                    <td><?php echo $dat["_desTut"]; ?></td>
                    <td><?php echo $dat["_idiTut"]; ?></td>
                    <td><?php echo isset($user) ? "<a href='" . $dat["_urlTut"] . "' target='_blank'>Ir</a>" : "Registrate o Entra"; ?></td>
                </tr>
                <?php
            }
        }
        ?>
    </table>
    <table>
        <tr>
            <td colspan="4" class="titP">Programacion - Otros</td>
        </tr>
        <tr>
            <td class="tdtit titS">Titulo</td>
            <td class="tddes titS">Descripcion</td>
            <td class="tdlink titS">Idioma</td>
            <td class="tdapr titS">Link</td>
        </tr>
<?php
$qSql = "select * from tb_tuthxc where _tipTut = 'pgr'";
$eSql = mysql_query($qSql);
if (mysql_error()) {
    echo showMenErr("Error en el servidor", "En estos momentos se ha producido un error en el servidor, por favor recarga la pagina y si aun no se soluciona contacta con el administrador...", "500", "<a href='javascript:document.reload();'>Recargar web</a>");
} else {
    while ($dat = mysql_fetch_array($eSql)) {
        ?>
                <tr>
                    <td><?php echo $dat["_titTut"]; ?></td>
                    <td><?php echo $dat["_desTut"]; ?></td>
                    <td><?php echo $dat["_idiTut"]; ?></td>
                    <td><?php echo isset($user) ? "<a href='" . $dat["_urlTut"] . "' target='_blank'>Ir</a>" : "Registrate o Entra"; ?></td>
                </tr>
        <?php
    }
}
?>
    </table>
    <br>
</div>
