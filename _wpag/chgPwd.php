<?php
session_start();
if (isset($_SESSION["_userHxC"]) && $_SESSION["_userHxC"] != "") {
    $user = $_SESSION["_userHxC"];
} else {
    header("Location: ./index.php");
}
?>
<html>
    <head>
        <script src="../_js/jquery-1.7.min.js" type="text/javascript"></script>
        <script>
            $(document).ready(function()
            {
                $("#frmCPass").submit(function()
                {
                    if ($("#txtNPwD").val() != $("#txtNPwDV").val())
                    {
                        alert("Las contrase" + String.fromCharCode(241) + "as no coinciden...");
                        $("#txtNPwD").val("");
                        $("#txtNPwDV").val("")
                        $("#txtNPwD").focus();
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                });
            });
        </script>
        <style>
            body { background-color: #000; color: #790000; text-align: center; }
            .field { border: 1px #1A1A1A solid; background: #414141; margin-right: 5px; margin-top: 4px; width: 180px; color: white; height: 20px; }
            .field:focus { border: 1px #0000FF solid; background: #545454; }
            .but { border: 1px #1A1A1A solid; background: #414141; margin-right: 5px; margin-top: 4px; width: 100px; color: white; height: 32px; }
            .but:focus { border: 1px #0000FF dotted; background: #545454; }
        </style>
        <title>Wargame - HackXCrack (Cambio de Contrase&ntilde;a)</title>
    </head>
    <body>
        <br />
<?php if (isset($_GET["tip"]) && $_GET["tip"] == "P") {
    ?>
            <form id="frmCPass" method="post" action="../_php/_ctr/ctrUsuario.php?mth=C">
                <table width="400px" border="0" align="center">
                    <tr>
                        <td width="49%" height="43">Antigua Contrase&ntilde;a: </td>
                        <td width="51%"><input name="txtAPwD" type="password" class="field"></td>
                    </tr>
                    <tr>
                        <td height="37">Nueva Contrase&ntilde;a: </td>
                        <td><input name="txtNPwD" id="txtNPwD" type="password" class="field"></td>
                    </tr>
                    <tr>
                        <td height="47">Verifique Nueva Contrase&ntilde;a: </td>
                        <td><input name="txtNPwDV" id="txtNPwDV" type="password" class="field"></td>
                    </tr>
                    <tr>
                        <td height="47" colspan="2" align="center"><label>
                                <input type="hidden" name="hidUsu" value="<?php echo isset($user) ? $user : ""; ?>">
                                <input name="Submit" type="submit" class="but" value="Ch4ng3 PwD">
                            </label></td>
                    </tr>
                </table>
            </form>
    <?php
} else if (isset($_GET["tip"]) && $_GET["tip"] == "I") {
    ?>
            <form method="post" action="../_php/_ctr/ctrUsuario.php?mth=I">
                <table width="400px" border="0" align="center">
                    <tr>
                        <td width="49%" height="43">Contrase&ntilde;a: </td>
                        <td width="51%"><input name="txtPwD" type="password" class="field"></td>
                    </tr>
                    <tr>
                        <td height="37">Url de la Nueva Imagen: </td>
                        <td><input name="txtNImG" type="text" class="field"></td>
                    </tr>
                    <tr>
                        <td height="47" colspan="2" align="center"><label>
                                <input type="hidden" name="hidUsu" value="<?php echo isset($user) ? $user : ""; ?>">
                                <input name="Submit" type="submit" class="but" value="Ch4ng3 ImG">
                            </label></td>
                    </tr>
                </table>
            </form>
    <?php
} else {
    echo "<center>Parametro invalido</center>";
}
?>
    </body>
</html>