<?php
session_start();
if (isset($_SESSION["_userHxC"]) && $_SESSION["_userHxC"] != "") {
    $user = $_SESSION["_userHxC"];
}
include("../_php/_cls/clsConex.php");
?>
<!-- <html>
<head>
<title></title>

</head>
<body> -->
<style type="text/css">
    table { border: 0px; margin: 2px; }
    td { border: 1px #790000 solid; padding: 1px; text-align: justify; }
    .titP { text-align: right; color: #AAAAAA; font-weight: bold; font-size: 12px; border: 0px; border-bottom: 2px #790000 dashed;  }
    .titS { color: #DDDDDD; font-weight: bold; background-color: #790000; }
    td img { width: 20px; height: 20px; position: relative; left: 50%; margin-left: -10px; }
</style>
<div align="center">
    <table>
        <tr>
            <td colspan="4" class="titP">SQL Injection y Blind SQL </td>
        </tr>
        <tr>
            <td class="tdtit titS">Titulo</td>
            <td class="tddes titS">Descripci&oacute;n</td>
            <td class="tdlink titS">Link</td>
            <td class="tdapr titS">Hecho</td>
        </tr>
        <?php
        $qSql = "select * from tb_warghxc where _tipWar = 'sqli'";
        $eSql = mysql_query($qSql);
        if (mysql_error()) {
            echo showMenErr("Error en el servidor", "En estos momentos se ha producido un error en el servidor, por favor recarga la pagina y si aun no se soluciona contacta con el administrador...", "500", "<a href='javascript:document.reload();'>Recargar web</a>");
        } else {
            while ($dat = mysql_fetch_array($eSql)) {
                ?>
                <tr>			
                    <td><?php echo $dat["_titWar"]; ?></td>
                    <td><?php echo $dat["_despWar"] . "&nbsp;<em>(" . $dat["_punWar"] . " puntos)</em>"; ?></td>
                    <td><?php if (isset($user)) { ?><a href="javascript:showEmer('./_warg/index.php?id=<?php echo $dat["_idWar"]; ?>', '700','400');">Ir</a><?php } else {
            echo "Debes Ingresar";
        } ?></td>
                    <td>
        <?php
        if (isset($user)) {
            $qSql2 = "select * from tb_wxuhxc where _nickUsu = '" . $user . "' and _idWar = '" . $dat["_idWar"] . "'";
            $eSql2 = mysql_query($qSql2);
            $cSql2 = @mysql_num_rows($eSql2);
            if ($cSql2 == 1) {
                ?>
                                <img src="./_img/acc.png" alt="resuelto" title="Resuelto">
                                <?php
                            } else {
                                ?>
                                <img src="./_img/err.png" alt="No resuelto" title="No resuelto">
                            <?php }
                        } else {
                            echo "O Registrarte";
                        } ?>	  
                    </td>
                </tr>
                        <?php
                    }
                }
                ?>
    </table>
    <br>
    <table>
        <tr>
            <td colspan="4" class="titP">RFI - LFI - XSS</td>
        </tr>
        <tr>
            <td class="tdtit titS">Titulo</td>
            <td class="tddes titS">Descripci&oacute;n</td>
            <td class="tdlink titS">Link</td>
            <td class="tdapr titS">Hecho</td>
        </tr>
<?php
$qSql = "select * from tb_warghxc where _tipWar = 'rlfi'";
$eSql = mysql_query($qSql);
if (mysql_error()) {
    echo showMenErr("Error en el servidor", "En estos momentos se ha producido un error en el servidor, por favor recarga la pagina y si aun no se soluciona contacta con el administrador...", "500", "<a href='javascript:document.reload();'>Recargar web</a>");
} else {
    while ($dat = mysql_fetch_array($eSql)) {
        ?>
                <tr>			
                    <td><?php echo $dat["_titWar"]; ?></td>
                    <td><?php echo $dat["_despWar"] . "&nbsp;<em>(" . $dat["_punWar"] . " puntos)</em>"; ?></td>
                    <td><?php if (isset($user)) { ?><a href="javascript:showEmer('./_warg/index.php?id=<?php echo $dat["_idWar"]; ?>', '700','400');">Ir</a><?php } else {
            echo "Debes Ingresar";
        } ?></td>
                    <td>
        <?php
        if (isset($user)) {
            $qSql2 = "select * from tb_wxuhxc where _nickUsu = '" . $user . "' and _idWar = '" . $dat["_idWar"] . "'";
            $eSql2 = mysql_query($qSql2);
            $cSql2 = @mysql_num_rows($eSql2);
            if ($cSql2 == 1) {
                ?>
                                <img src="./_img/acc.png" alt="resuelto" title="Resuelto">
                                <?php
                            } else {
                                ?>
                                <img src="./_img/err.png" alt="No resuelto" title="No resuelto">
                            <?php }
                        } else {
                            echo "O Registrarte";
                        } ?>	  
                    </td>
                </tr>
                        <?php
                    }
                }
                ?>
    </table>
    <br>
    <table>
        <tr>
            <td colspan="4" class="titP">CriptoGraf&iacute;a y Esteganograf&iacute;a</td>
        </tr>
        <tr>
            <td class="tdtit titS">Titulo</td>
            <td class="tddes titS">Descripci&oacute;n</td>
            <td class="tdlink titS">Link</td>
            <td class="tdapr titS">Hecho</td>
        </tr>
<?php
$qSql = "select * from tb_warghxc where _tipWar = 'crypt'";
$eSql = mysql_query($qSql);
if (mysql_error()) {
    echo showMenErr("Error en el servidor", "En estos momentos se ha producido un error en el servidor, por favor recarga la pagina y si aun no se soluciona contacta con el administrador...", "500", "<a href='javascript:document.reload();'>Recargar web</a>");
} else {
    while ($dat = mysql_fetch_array($eSql)) {
        ?>
                <tr>			
                    <td><?php echo $dat["_titWar"]; ?></td>
                    <td><?php echo $dat["_despWar"] . "&nbsp;<em>(" . $dat["_punWar"] . " puntos)</em>"; ?></td>
                    <td><?php if (isset($user)) { ?><a href="javascript:showEmer('./_warg/index.php?id=<?php echo $dat["_idWar"]; ?>', '700','400');">Ir</a><?php } else {
            echo "Debes Ingresar";
        } ?></td>
                    <td>
                <?php
                if (isset($user)) {
                    $qSql2 = "select * from tb_wxuhxc where _nickUsu = '" . $user . "' and _idWar = '" . $dat["_idWar"] . "'";
                    $eSql2 = mysql_query($qSql2);
                    $cSql2 = @mysql_num_rows($eSql2);
                    if ($cSql2 == 1) {
                        ?>
                                <img src="./_img/acc.png" alt="resuelto" title="Resuelto">
                                <?php
                            } else {
                                ?>
                                <img src="./_img/err.png" alt="No resuelto" title="No resuelto">
                            <?php }
                        } else {
                            echo "O Registrarte";
                        } ?>	  
                    </td>
                </tr>
                        <?php
                    }
                }
                ?>
    </table>
    <br>
    <table>
        <tr>
            <td colspan="4" class="titP">Ingenieria Inversa (BF)</td>
        </tr>
        <tr>
            <td class="tdtit titS">Titulo</td>
            <td class="tddes titS">Descripci&oacute;n</td>
            <td class="tdlink titS">Link</td>
            <td class="tdapr titS">Hecho</td>
        </tr>
<?php
$qSql = "select * from tb_warghxc where _tipWar = 'IngI'";
$eSql = mysql_query($qSql);
if (mysql_error()) {
    echo showMenErr("Error en el servidor", "En estos momentos se ha producido un error en el servidor, por favor recarga la pagina y si aun no se soluciona contacta con el administrador...", "500", "<a href='javascript:document.reload();'>Recargar web</a>");
} else {
    while ($dat = mysql_fetch_array($eSql)) {
        ?>
                <tr>			
                    <td><?php echo $dat["_titWar"]; ?></td>
                    <td><?php echo $dat["_despWar"] . "&nbsp;<em>(" . $dat["_punWar"] . " puntos)</em>"; ?></td>
                    <td><?php if (isset($user)) { ?><a href="javascript:showEmer('./_warg/index.php?id=<?php echo $dat["_idWar"]; ?>', '700','400');">Ir</a><?php } else {
            echo "Debes Ingresar";
        } ?></td>
                    <td>
                <?php
                if (isset($user)) {
                    $qSql2 = "select * from tb_wxuhxc where _nickUsu = '" . $user . "' and _idWar = '" . $dat["_idWar"] . "'";
                    $eSql2 = mysql_query($qSql2);
                    $cSql2 = @mysql_num_rows($eSql2);
                    if ($cSql2 == 1) {
                        ?>
                                <img src="./_img/acc.png" alt="resuelto" title="Resuelto">
                                <?php
                            } else {
                                ?>
                                <img src="./_img/err.png" alt="No resuelto" title="No resuelto">
                            <?php }
                        } else {
                            echo "O Registrarte";
                        } ?>	  
                    </td>
                </tr>
                        <?php
                    }
                }
                ?>
    </table>
    <br>
    <table>
        <tr>
            <td colspan="4" class="titP">Programacion - Otros</td>
        </tr>
        <tr>
            <td class="tdtit titS">Titulo</td>
            <td class="tddes titS">Descripci&oacute;n</td>
            <td class="tdlink titS">Link</td>
            <td class="tdapr titS">Hecho</td>
        </tr>
<?php
$qSql = "select * from tb_warghxc where _tipWar = 'pgr'";
$eSql = mysql_query($qSql);
if (mysql_error()) {
    echo showMenErr("Error en el servidor", "En estos momentos se ha producido un error en el servidor, por favor recarga la pagina y si aun no se soluciona contacta con el administrador...", "500", "<a href='javascript:document.reload();'>Recargar web</a>");
} else {
    while ($dat = mysql_fetch_array($eSql)) {
        ?>
                <tr>			
                    <td><?php echo $dat["_titWar"]; ?></td>
                    <td><?php echo $dat["_despWar"] . "&nbsp;<em>(" . $dat["_punWar"] . " puntos)</em>"; ?></td>
                    <td><?php if (isset($user)) { ?><a href="javascript:showEmer('./_warg/index.php?id=<?php echo $dat["_idWar"]; ?>', '700','400');">Ir</a><?php } else {
            echo "Debes Ingresar";
        } ?></td>
                    <td>
                <?php
                if (isset($user)) {
                    $qSql2 = "select * from tb_wxuhxc where _nickUsu = '" . $user . "' and _idWar = '" . $dat["_idWar"] . "'";
                    $eSql2 = mysql_query($qSql2);
                    $cSql2 = @mysql_num_rows($eSql2);
                    if ($cSql2 == 1) {
                        ?>
                                <img src="./_img/acc.png" alt="resuelto" title="Resuelto">
                <?php
            } else {
                ?>
                                <img src="./_img/err.png" alt="No resuelto" title="No resuelto">
                            <?php }
                        } else {
                            echo "O Registrarte";
                        } ?>	  
                    </td>
                </tr>
                        <?php
                    }
                }
                ?>
    </table>
    <br>
    <table>
        <tr>
            <td colspan="4" class="titP">Hacker del Mes</td>
        </tr>
        <tr>
            <td class="tdtit titS">Titulo</td>
            <td class="tddes titS">Descripci&oacute;n</td>
            <td class="tdlink titS">Link</td>
            <td class="tdapr titS">Hecho</td>
        </tr>
<?php
$qSql = "select * from tb_warghxc where _tipWar = 'hxm'";
$eSql = mysql_query($qSql);
if (mysql_error()) {
    echo showMenErr("Error en el servidor", "En estos momentos se ha producido un error en el servidor, por favor recarga la pagina y si aun no se soluciona contacta con el administrador...", "500", "<a href='javascript:document.reload();'>Recargar web</a>");
} else {
    while ($dat = mysql_fetch_array($eSql)) {
        ?>
                <tr>			
                    <td><?php echo $dat["_titWar"]; ?></td>
                    <td><?php echo $dat["_despWar"] . "&nbsp;<em>(" . $dat["_punWar"] . " puntos)</em>"; ?></td>
                    <td><?php if (isset($user)) { ?><a href="javascript:showEmer('./_warg/index.php?id=<?php echo $dat["_idWar"]; ?>', '700','400');">Ir</a><?php } else {
            echo "Debes Ingresar";
        } ?></td>
                    <td>
                <?php
                if (isset($user)) {
                    $qSql2 = "select * from tb_wxuhxc where _nickUsu = '" . $user . "' and _idWar = '" . $dat["_idWar"] . "'";
                    $eSql2 = mysql_query($qSql2);
                    $cSql2 = @mysql_num_rows($eSql2);
                    if ($cSql2 == 1) {
                        ?>
                                <img src="./_img/acc.png" alt="resuelto" title="Resuelto">
                <?php
            } else {
                ?>
                                <img src="./_img/err.png" alt="No resuelto" title="No resuelto">
                            <?php }
                        } else {
                            echo "O Registrarte";
                        } ?>	  
                    </td>
                </tr>
                        <?php
                    }
                }
                ?>
    </table>
</div>
<!--</body>
</html>-->