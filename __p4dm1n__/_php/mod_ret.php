<?php
session_start();
if (!isset($_SESSION["4dm1n"])) {
    header("Location: ../index.php");
} else if ($_SESSION["4dm1n"] == "") {
    header("Location: ../index.php");
} else {
    error_reporting(0);
    include_once("./clsConex.php");
    $id = mysql_real_escape_string($_GET["id"]);
    $sql = "select * from tb_warghxc where _idWar = " . $id;
    $eSql = mysql_query($sql);
    $dat = mysql_fetch_array($eSql);
}
?>
<div align="center">
    <form method="post" action="./_php/cls_modret.php">
        <table width="396" border="0">
            <tr>
                <td height="41" colspan="2">Modificar Reto (<a href="#" onclick="window.open('./_php/upload.php', '', 'toolbar=no, location=no, directories=yes, status=no, menubar=no, scrollbars=yes, resizable=no, width=450, height=200, top=85, left=140');">Upload</a>)</td>
            </tr>
            <tr>
                <td width="221">Titulo:</td>
                <td width="269">
                    <input type="text" name="txtTit" value="<?php echo $dat["_titWar"]; ?>" /></td>
            </tr>
            <tr>
                <td>Tipo: </td>
                <td>
                    <select name="selTip">
                        <option value="non">Elije...</option>
                        <option value="sqli" <?php echo $dat["_tipWar"] == "sqli" ? "selected='selected'" : ""; ?>>SQL Injection</option>
                        <option value="pgr" <?php echo $dat["_tipWar"] == "pgr" ? "selected='selected'" : ""; ?>>Programacion y Otros</option>
                        <option value="rlfi" <?php echo $dat["_tipWar"] == "rlfi" ? "selected='selected'" : ""; ?>>Remote/Local File Inclusion - XSS</option>
                        <option value="IngI" <?php echo $dat["_tipWar"] == "IngI" ? "selected='selected'" : ""; ?>>Ingenieria Inversa</option>
                        <option value="crypt" <?php echo $dat["_tipWar"] == "crypt" ? "selected='selected'" : ""; ?>>Cripto/EsteganoGrafia</option>
                        <option value="hxm" <?php echo $dat["_tipWar"] == "hxm" ? "selected='selected'" : ""; ?>>Hacker del Mes</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td>Descripci&oacute;n del reto:</td>
                <td>
                    <textarea name="txtDes" cols="40" rows="3" style='resize: none;'><?php echo $dat["_desWar"]; ?></textarea>
                </td>
            </tr>
            <tr>
                <td>Descripci&oacute;n p&uacute;blica:</td>
                <td>
                    <textarea name="txtDesP" cols="40" rows="3" style='resize: none;'><?php echo $dat["_despWar"]; ?></textarea>
                </td>
            </tr>
            <tr>
                <td>Creador:</td>
                <td>
                    <input type="text" name="txtCre" value="<?php echo $dat["_creWar"]; ?>" />
                </td>
            </tr>
            <tr>
                <td>Promtp???:</td>
                <td style='padding-top: 0px;'>
                    <label for="promtyes" style='position: absolute; margin-top: 8px;'>Si</label><input type="radio" name="rbtPrompt" id="promtyes" value="yes" checked="checked" /><br>
                    <label for="promtno" style='position: absolute; margin-top: 8px;'>No</label><input type="radio" name="rbtPrompt" id="promtno" value="no"  <?php echo $dat["_prompt"] == "no" ? "checked='checked'" : ""; ?>/>
                </td>
            </tr>
            <tr>
                <td>Puntuaci&oacute;n (Min 1 - Max 10):</td>
                <td>
                    <input type="text" name="txtPun" value="<?php echo $dat["_punWar"]; ?>" />
                </td>
            </tr>
            <tr>
                <td>Attributos del input:</td>
                <td>
                    <input type="text" name="txtAtt" value="<?php echo htmlentities($dat["_attEsp"]); ?>" />
                </td>
            </tr>
            <tr>
                <td>Soluci&oacute;n:</td>
                <td>
                    <input type="text" name="txtSol" value="<?php echo $dat["_solWar"]; ?>"/>
                </td>
            </tr>
            <tr>
                <td height="49" colspan="2">
                    <input type="hidden" value="<?php echo $dat["_idWar"]; ?>" name="idWar_">
                    <input type="submit" name="btnAgr" value="Enviar" />
                </td>
            </tr>
        </table>
    </form>
</div>
