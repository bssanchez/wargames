<?php
session_start();
if (!isset($_SESSION["4dm1n"])) {
    header("Location: ../index.php");
} else if ($_SESSION["4dm1n"] == "") {
    header("Location: ../index.php");
}
?>
<div align="center">
    <form method="post" action="./_php/cls_tut.php">
        <table width="396" border="0">
            <tr>
                <td height="41" colspan="2">Nuevo Tutorial</td>
            </tr>
            <tr>
                <td width="221">Titulo:</td>
                <td width="269">
                    <input type="text" name="txtTit" /></td>
            </tr>
            <tr>
                <td>URL:</td>
                <td>
                    <input type="text" name="txtUrl" />
                </td>
            </tr>
            <tr>
                <td>Idioma:</td>
                <td>
                    <select name="selIdi">
                        <option value="Espanol">Espa&ntilde;ol</option>
                        <option value="Ingles">Ingles</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td>Tipo:</td>
                <td>
                    <select name="selTip">
                        <option value="">Elije...</option>
                        <option value="sqli">Inyeccion SQL y/o Blind</option>
                        <option value="pgr">Programaci&oacute;n y/o Otros</option>
                        <option value="rlx">Remote/Local File Inclusion y/o XSS</option>
                        <option value="IngI">Ingenieria Inversa</option>
                        <option value="crypt">Criptografia y/o Esteganografia</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td colspan="2">Descripcion:<br>
                    <textarea name="txtDes" cols="40" rows="5"></textarea>
                </td>
            </tr>
            <tr>
                <td height="49" colspan="2">
                    <input type="submit" name="btnAgr" value="Enviar" />
                </td>
            </tr>
        </table>
    </form>
</div>
