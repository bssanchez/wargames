<?php
session_start();
if (!isset($_SESSION["4dm1n"])) {
    header("Location: ../index.php");
} else if ($_SESSION["4dm1n"] == "") {
    header("Location: ../index.php");
} else {
    ?>
    <html>
        <head>
            <title>Upload :P</title>
        </head>
        <body style="background-color: #000; color: #0F5">
    <?php
    $target = array(
        "sqli" => "../../_warg/SQLi/",
        "pgr" => "../../_warg/PGR/",
        "rlfi" => "../../_warg/RLFI/",
        "IngI" => "../../_warg/CRK/",
        "crypt" => "../../_warg/Crypt/"
    );
    if (isset($_POST['btn_Env']) && $_POST['selTip'] != "non" && basename($_FILES['archivo']['name']) != "") {
        $tip = $_POST['selTip'];
        $guardar = $target[$tip];

        $tp = $guardar . str_replace(" ", "_", basename($_FILES['archivo']['name']));

        if (file_exists($tp) && $_POST['rbtR'] == 'no') {
            echo "Este archivo ya existe, por favor cambie el nombre...";
            echo "<br><br><a href='upload.php'>Volver</a>";
        } else if (move_uploaded_file($_FILES['archivo']['tmp_name'], $tp)) {
            $href = htmlentities("<a href='" . str_replace("../../_warg/", "./", $tp) . "'>Descargar/Abrir</a>");
            echo "Guardado correctamente... la url que debes poner es: ";
            echo "<pre>" . $href . "</pre>";
            echo "<br><br><a href='upload.php'>Volver</a>";
        } else {
            echo "Error el archivo no se subio :/";
            echo "<br><br><a href='upload.php'>Volver a intentar</a>";
        }
    } else {
        echo "<pre>No se ha enviado nada o no se ha puesto tipo...</pre>";
        ?>
                <form enctype="multipart/form-data" action="upload.php" method="post">
                    Seccion:
                    <select name="selTip">
                        <option value="non">Elije...</option>
                        <option value="sqli">SQL Injection</option>
                        <option value="pgr">Programacion y Otros</option>
                        <option value="rlfi">Remote/Local File Inclusion - XSS</option>
                        <option value="IngI">Ingenieria Inversa</option>
                        <option value="crypt">Cripto/EsteganoGrafia</option>
                    </select>
                    <br><br>
                    Archivo: <input type="file" name="archivo" id="archivo">
                    Reemplazar: <input type="radio" name="rbtR" value="si">SI - <input type="radio" name="rbtR" value="no" checked="checked">NO
                    <br><br>
                    <input type="submit" name="btn_Env" value="Subir!!!">
                </form>
        <?php
    }
    ?>
        </body>
    </html>
    <?php
}
?>
