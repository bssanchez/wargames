<?php
session_start();
if (!isset($_SESSION["4dm1n"])) {
    header("Location: ../index.php");
} else if ($_SESSION["4dm1n"] == "") {
    header("Location: ../index.php");
}
?>
<div align="center">
    <form method="post" action="./_php/cls_ret.php">
        <table width="396" border="0">
            <tr>
                <td height="41" colspan="2">Nuevo Reto (<a href="#" onclick="window.open('./_php/upload.php', '', 'toolbar=no, location=no, directories=yes, status=no, menubar=no, scrollbars=yes, resizable=no, width=450, height=200, top=85, left=140');">Upload</a>)</td>
            </tr>
            <tr>
                <td width="221">Titulo:</td>
                <td width="269">
                    <input type="text" name="txtTit" /></td>
            </tr>
            <tr>
                <td>Tipo: </td>
                <td>
                    <select name="selTip">
                        <option value="non">Elije...</option>
                        <option value="sqli">SQL Injection</option>
                        <option value="pgr">Programacion y Otros</option>
                        <option value="rlfi">Remote/Local File Inclusion - XSS</option>
                        <option value="IngI">Ingenieria Inversa</option>
                        <option value="crypt">Cripto/EsteganoGrafia</option>
                        <option value="hxm">Hacker del Mes</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td>Descripci&oacute;n del reto:</td>
                <td>
                    <textarea name="txtDes" cols="40" rows="3" style='resize: none;'></textarea>
                </td>
            </tr>
            <tr>
                <td>Descripci&oacute;n p&uacute;blica:</td>
                <td>
                    <textarea name="txtDesP" cols="40" rows="3" style='resize: none;'></textarea>
                </td>
            </tr>
            <tr>
                <td>Creador:</td>
                <td>
                    <input type="text" name="txtCre" />
                </td>
            </tr>
            <tr>
                <td>Promtp???:</td>
                <td style='padding-top: 0px;'>
                    <label for="promtyes" style='position: absolute; margin-top: 8px;'>Si</label><input type="radio" name="rbtPrompt" id="promtyes" value="yes" checked="checked" /><br>
                    <label for="promtno" style='position: absolute; margin-top: 8px;'>No</label><input type="radio" name="rbtPrompt" id="promtno" value="no" />
                </td>
            </tr>
            <tr>
                <td>Puntuaci&oacute;n (Min 1 - Max 10):</td>
                <td>
                    <input type="text" name="txtPun" />
                </td>
            </tr>
            <tr>
                <td>Attributos del input:</td>
                <td>
                    <input type="text" name="txtAtt" />
                </td>
            </tr>
            <tr>
                <td>Soluci&oacute;n:</td>
                <td>
                    <input type="text" name="txtSol" />
                </td>
            </tr>
            <tr>
                <td height="49" colspan="2">
                    <input type="submit" name="btnAgr" value="Enviar" />
                </td>
            </tr>
        </table>
    </form>
</div>
