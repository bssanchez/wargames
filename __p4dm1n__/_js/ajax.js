// JavaScript Document

//-->CODIGO  AJAX-----------------------------------------------------------------------------------------------------
function nuevoAjax() {
    var xmlhttp = false;
    try
    {
        xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
    }
    catch (e)
    {
        try
        {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        catch (E)
        {
            xmlhttp = false;
        }
    }

    if (!xmlhttp && typeof XMLHttpRequest != 'undefined')
    {
        xmlhttp = new XMLHttpRequest();
    }
    return xmlhttp;
}

function wp(pag)
{
    var contenedor;
    contenedor = document.getElementById('pag');
    contenedor.innerHTML = '<div align="center"><p/><p/><p/><img src="./_css/cargando.gif" align="absmiddle" border="0" /></div>';
    ajax = nuevoAjax();
    ajax.open("GET", pag, true);
    ajax.onreadystatechange = function() {
        if (ajax.readyState == 4)
        {
            contenedor.innerHTML = ajax.responseText;
        }
    }
    ajax.send(null)
}