-- MySQL dump 10.13  Distrib 5.5.28, for Linux (x86_64)
--
-- Host: localhost    Database: db_warhxc
-- ------------------------------------------------------
-- Server version	5.5.28

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tb_tuthxc`
--

DROP TABLE IF EXISTS `tb_tuthxc`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_tuthxc` (
  `_idTut` int(11) NOT NULL AUTO_INCREMENT,
  `_titTut` varchar(50) NOT NULL,
  `_urlTut` varchar(200) NOT NULL,
  `_tipTut` varchar(10) NOT NULL,
  `_idiTut` varchar(20) NOT NULL,
  `_desTut` text NOT NULL,
  PRIMARY KEY (`_idTut`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_tuthxc`
--

LOCK TABLES `tb_tuthxc` WRITE;
/*!40000 ALTER TABLE `tb_tuthxc` DISABLE KEYS */;
INSERT INTO `tb_tuthxc` VALUES (1,'Cifrado Bifid','http://www.hackxcrack.es/forum/index.php?topic=7318','cri','Espa&ntilde;ol','Un peque&ntilde;o tutorial sobre este cifrado... solo es una peque&ntilde;a entremesa xD...'),(2,'SQL Injection','http://www.hackxcrack.es/cuadernos/sql/','sql','Espa&ntilde;ol','Este tuto fue creado por kenkeiras para hack x crack... Saludos y Buena Suerte xD...'),(3,'XSS AIO','http://xsser.sourceforge.net/xsser/XSS_for_fun_and_profit_SCG09.pdf','rlx','Espa&ntilde;ol','Excelente Libro con los ataques de XSS mas comunes y menos xDDD...'),(4,'Reco. de Cracking','http://www.hackxcrack.es/forum/index.php?topic=1057.0','iiv','Espa&ntilde;ol','Una muy recopilacion de tutoriales y herramientas para practicar Ingenieria Inversa... Post publicado por 3hy!...'),(5,'Prog. VB6','http://www.hackxcrack.es/forum/index.php?topic=450.0','pgr','Espa&ntilde;ol','Aporte entregado por nuestro amigacho 3hy!, y que mejor que empezar con este lenguaje xD... Saludos'),(6,'SQLi Avanzado','http://www.hackxcrack.es/forum/index.php?topic=297.0','sql','Espa&ntilde;ol','Un buen tutorial creado por un amigacho de la comunidad...'),(7,'CSRF - Cross Site Request Forgery','https://foro.undersecurity.net/read.php?76,6258','rlx','Espa&ntilde;ol','Buen tutorial acerca de esta vulnerabilidad web... creado por el Guason');
/*!40000 ALTER TABLE `tb_tuthxc` ENABLE KEYS */;
UNLOCK TABLES;


--
-- Table structure for table `tb_userhxc`
--

DROP TABLE IF EXISTS `tb_userhxc`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_userhxc` (
  `_idUsu` int(11) NOT NULL AUTO_INCREMENT,
  `_nickUsu` varchar(30) NOT NULL,
  `_nomUsu` varchar(30) NOT NULL,
  `_corUsu` varchar(200) NOT NULL,
  `_pasUsu` varchar(40) NOT NULL,
  `_imgUsu` varchar(200) NOT NULL,
  `_preSeg` int(2) NOT NULL,
  `_resSeg` varchar(40) NOT NULL,
  `_punUser` int(11) NOT NULL,
  `_fecUsu` bigint(20) NOT NULL,
  `_HxM` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`_idUsu`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tb_warghxc`
--

DROP TABLE IF EXISTS `tb_warghxc`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_warghxc` (
  `_idWar` int(11) NOT NULL AUTO_INCREMENT,
  `_titWar` varchar(50) NOT NULL,
  `_tipWar` varchar(10) NOT NULL,
  `_desWar` text NOT NULL,
  `_despWar` text NOT NULL,
  `_creWar` varchar(100) NOT NULL,
  `_punWar` int(11) NOT NULL,
  `_prompt` varchar(3) NOT NULL DEFAULT 'yes',
  `_attEsp` text NOT NULL,
  `_solWar` varchar(100) NOT NULL,
  `_HxM` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`_idWar`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_warghxc`
--

LOCK TABLES `tb_warghxc` WRITE;
/*!40000 ALTER TABLE `tb_warghxc` DISABLE KEYS */;
INSERT INTO `tb_warghxc` VALUES (1,'Atacando un Buscador [1]','sqli','Cierto personaje de la red creo un buscador no muy util... busca usuarios por usuario. Ya se ha detectado que es vulnerable a SQL Injection y que hay un usuario admin, tu mision es sacar la pass de ese admin y darnosla... xD y Suerte (<a href=\"./SQLi/buscador-v1.0.php\">Ir al Buscador</a>)','Cierto personaje de la red creo un buscador no muy util... busca usuarios por usuario. Ya se ha detectado que es vulnerable a SQL Injection y que hay un usuario admin, tu mision es sacar la pass de ese admin y darnosla.','kid_goth',6,'yes','','17b9ea80c8822ca6ae8b3c72bbfa0845',0),(2,'Busca la Password','pgr','En este reto simplemente debes averiguar una password y escribirla en el campo de texto... Suerte ;)<span style=\'color:#fff; font-size: 5px;\'>testdeprog1</span>','En este reto simplemente debes averiguar una password y escribirla en el campo de texto.','kid_goth',3,'yes','readonly=\"readonly\"','173de728fb4f20a0a49385e975ed46dc',0),(4,'XSS - Encuesta','rlfi','En este reto solo debes Manipular el envio de datos del formulario de la encuesta para hacer el ataque XSS... vamos a que puedes y Suerte. (<a href=\'./RLFI/xss1.php\'>Entrar</a>)','En este reto solo debes Manipular el envio de datos del formulario de la encuesta para hacer el ataque XSS.','mrobles',5,'no','','86734578dc9dc665da379a3152c36b63',0),(5,'CrackMe 1','IngI','Como desarrollarlo?, Simple descargar el archivo, conseguir la contrase&ntilde;a y enviarla con el formulario... Suerte -> (<a href=\"./CRK/crackme1.exe\">Descargar</a>)','Como desarrollarlo?, Simple descargar el archivo, conseguir la contrase&ntilde;a y enviarla con el formulario.','TzSpy',4,'yes','','b5e7cb72de532148b330bdd3390754a9',0),(6,'CrackMe 2','IngI','Como desarrollarlo?, Simple descargar el archivo, conseguir la contrase&ntilde;a y enviarla con el formulario... Suerte -> (<a href=\"./CRK/crackme2.exe\">Descargar</a>)','Como desarrollarlo?, Simple descargar el archivo, conseguir la contrase&ntilde;a y enviarla con el formulario.','TzSpy',4,'yes','','7215ee9c7d9dc229d2921a40e899ec5f',0),(7,'EstaganIN','crypt','Facilito, debes encontrar el mensaje oculto en la imagen por cualquier metodo... Saludos -> (<a href=\"./Crypt/image.bmp\">Imagen</a>)','Facilito, debes encontrar el mensaje oculto en la imagen por cualquier metodo.','kid_goth',6,'yes','','40ea6844a798c3d4e326208248d2681a',0),(8,'CriptoGrafia [1]','crypt','En este reto debes descifrar un texto que se ha cifrado con un metodo no muy conocido pero sencillo... Suerte<br>\r\n<i>1224154225  214453  134231112542553232  2154  31341123212532  4131142123  54  1145  32324142313253253555  31414525  24225335  42222532  2453  4131131322442123324452</i>','En este reto debes descifrar un texto que se ha cifrado con un metodo no muy conocido pero sencillo.','kid_goth',7,'yes','','c65271939b2bbae0869f9ac436176d7d',0),(9,'Obteniendo Acceso','pgr','En este reto solo debes obtener acceso a la web... por cualquier metodo xD. Suerte -> (<a href=\"./PGR/pgr2.php\">Entrar</a>)','En este reto solo debes obtener acceso a la web... por cualquier metodo xD.','kid_goth',5,'no','','04630299d41d8f4ed092be012a70eb3c',0),(10,'Panel del Admin','pgr','Danos la contrase&ntilde;a del usuario admin, es sencillo solo sigue las huellas ;) -> (<a href=\"./PGR/admin\">Entrar</a>)','Danos la contrase&ntilde;a del usuario admin, es sencillo solo sigue las huellas.','kid_goth',7,'yes','','1ea77c6cbbad84d4511090b48ee8d785',0),(11,'CriptoGrafia [2]','crypt','Bueno este es un metodo modificado por mi, resuelvelo, yo se que puedes... <br>Solo una pregunta: Si la forma cifrada de <b>CriptoGrafia</b> es <em>DtltyuNzjptm</em>, &iquest;cual es el hash para HackxCrack? (se tienen en cuenta mayusculas)','Bueno este es un metodo modificado por mi, resuelvelo, yo se que puedes.','kid_goth',5,'yes','','e8108d0e69759cd3e602b6ece981a7a1',0),(12,'Crackme Multiple [1]','IngI','Este reto se basa en tres niveles, este es el mas sencillo, danos la password del usuario MARCOSCARS02 -> (<a href=\"./CRK/crackme3.exe\">Descargar</a>)','Este reto se basa en tres niveles, este es el mas sencillo.','MARCOSCARS02',5,'yes','','842447c6635f96f3130a0956708cfe82',0),(13,'Crackme Multiple [2]','IngI','Este reto se basa en tres niveles, este es el nivel medio, danos la password del usuario KidGoth -> (<a href=\"./CRK/crackme3.exe\">Descargar</a>)','Este reto se basa en tres niveles, este es el nivel medio.','MARCOSCARS02',6,'yes','','8b39d074dcd6483a1a618a254a9a49c5',0),(14,'Crackme Multiple [3]','IngI','Este reto se basa en tres niveles, este es el nivel alto, danos la password del usuario HackXCrack -> (<a href=\"./CRK/crackme3.exe\">Descargar</a>)','Este reto se basa en tres niveles, este es el nivel alto.','MARCOSCARS02',7,'yes','','b2af1afb75a120f96fa63f750c8ed2f8',0),(15,'HxM - Abril','hxm','Debes darnos la password de chuck... esperamos tu respuesta pronto ;) -> (<a href=\"./HxM/abril/\">Entrar</a>)','El juego de este mes es sencillo ya que apenas empezamos, recuerda en este reto el primero que lo solucione tendra 30 puntos y la corona.','kid_goth',15,'yes','','6cbad22cdf655e9d49895421be36e118',0),(16,'Estegano Fail','crypt','Rayos estaba intentando crear el reto pero no me salio como queria... espero lo puedas arreglar ;) -> (<a href=\"./Crypt/stgFail.jpg\">Imagen</a>)','En este reto como siempre buscar lo oculto, con la diferencia de que no salio como queria :P.','kid_goth',4,'yes','','ac08355d96a274434b8d460f14af5d6b',0),(17,'OverCrackMe [1]','IngI','De nuevo un reto multiple, vamos danos la password xD -> (<a href=\"./CRK/overcrackme.exe\">Descargar</a>)','Miyamoto nos ha dado un excelente material, resolvedlo que este es el nivel facil.','Miyamoto',4,'yes','','75dd98c6a57e832b2e608648a0eb6b2e',0),(18,'OverCrackMe [2]','IngI','Comete a Miyamoto y cambia la password -> (<a href=\"./CRK/overcrackme.exe\">Descargar</a>), pon de password sudoCRACKEATE! y entreganos lo que enviaste para lograrlo. (utiliza la letra <em>a</em> por favor)','No obstante trae 2 vulnerabilidades, resolvedlo que este es el nivel Dificil ;).','Miyamoto',7,'yes','','e89eb3f716820111873298d53ae1fb2e',0),(19,'Crackme JAR','IngI','Recuerda para este reto debes tener JAVA instalado, JRE para ser mas exactos y obviamente este mu&ntilde;equito xD -> (<a href=\"./CRK/crackmejar.jar\">Descargar</a>)','No solo los .exe tienen derecho a ser crackeados y aca esta la prueba :P.','cracklos7',4,'yes','','d3411781ea38a4771d07364a5750ba33',0),(20,'Atacando un Buscador [2]','sqli','Bueno igual que antes necesitamos la password de Kevin o del Admin (root) xD... Suerte -> (<a href=\"./SQLi/buscador-v2.0.php\">Ir al Buscador</a>)','Recuerdan al personaje que creo el buscador vulnerable??? pues supo de lo que haciamos y mejoro la seguridad.','kid_goth',8,'yes','','8aea624e3daeb007d19475e370cff61e',0),(21,'2 Caras','pgr','Bueno ni mas que decir de este reto danos la pass por cierto... -> (<a href=\"./PGR/ret2FaC.php\">Entrar</a>)','Abre tu mente al maximo que lo necesitaras...','2Fac3R',8,'yes','','3516387be271156ce7069ea09aaa69a7',0),(22,'Estegano','crypt','Vaya se ha logrado ocultar el \\\' delincuente \\\' y solo el Zorro lo puede vencer... ;) -> (<a href=\'./Crypt/leviatan1.bmp\'>Imagen</a>)','Leviatan nos trajo 2 retos empieza con este... seguro lo logras rapido','Leviatan',7,'yes','','3242628ca86e7214faa0f14a6f8fdf01',0),(23,'Estegano','crypt','QR uuu la nueva onda pero este combina los 2 tipos de reto, asi que pasa uno y ve por el siguiente -> (<a href=\\\"./Crypt/leviatan2.png\\\">Imagen</a>)','Segundo reto por leviatan este si que es complejo pero dale por esos ','Laviatan',9,'yes','','a2b10c0f7d79fc70aca25775b73f5ed0',0),(24,'HxM - Mayo','hxm','Se ha capturado a un tipo señalado de traficar pornografia infantil, en el momento de la captura llevaba con el un Disquete el cual se cree tiene las pruebas que lo llevaran a la carcel. Se hizo una imagen forense del Disquete y se te ha enviado para el analisis esperamos que lo logres. Se te entregan 2 archivos para que cumplas con el reto:<br><br>\r\n1ro La Img Forense en zip -> (<a href=\"./HxM/mayo/img_diskette.dd.zip\">Descargar</a>) md5_file = <em>c376c8aa5a232d92de9ef9f18edfe2f1</em><br>\r\n2do Descripción -> (<a href=\"./HxM/mayo/descripcion.txt\">Descargar</a>)... Suerte ;)','Bueno esta vez he fabricado un retillo forense... tomado de la idea de mrobles espero se tomen su tiempo para realizarlo...','kid_goth',15,'yes','','5368b4d34cd4e5545c5e1387597cb84b',0),(25,'Cifrado Multiple','crypt','Hola mi nick es Lafleur y me han dicho que mi pagina de informacion de usuarios no es segura pues cosa que no creo, pero he contratado al mejor hacker del mundo y ese eres tu revisa mi pagina y ve si puedes entrar a mi sesion. -> (<a href=\'./Crypt/Lafleur\'>Ir al Reto</a>)','mmmm yo digo que es muy complicado el dice que en media hora se hace a ver tu que dices ?? ;)','LaFleur',10,'yes','','bb5b8e74d1b487d61f171a1706f54e95',0),(26,'Criptografia Molto-Facil','crypt','Un d&#65533;a conversando con LaFleur me comento que estuvo por las tierras peruanas  visitando  machu picchu y observo a lo largo que unos seres extra&#65533;os dejaron un mensaje en las colinas del machupicchu. Espero que puedas decifrar el dichoso mensaje que tiene consternado a LaFleur.\r\n<a href=\'./Crypt/reto_cryto.jpg\'>Descargar/Abrir</a>','Bueno es una criptografia modificada; lafleur me dijo que es muy facil. crees que sera tan facil.averigualo..suerte..XD','kyr4-bl4ck',9,'yes','','9c00909a16429777fbee53ef12c19a23',0),(27,'HxM - Diciembre','hxm','...Fuentes confiables (los creadores del plugin) nos han dicho que aun es vulnerable asi que atacadlo ahora y demuestra que sos el mejor. Danos la password del root del sistema (<a href=\\\'./HxM/diciembre\\\'>Ir al reto</a>)','Los Buscadores cambiaron de administracion y al parecer se pagaron un plugin que protege el sitio de sqli pero...','prow',15,'yes','','416a9e4c65e42d813d9b479ba4c33311',0),(28,'Voz Diabolica','non','En las Ruinas de Chichen Itza Yucatan, encontraron una cinta de audio, segun los exploradoradores ha sido poseida por espiritus demoniacos la cual contamos con tu ayuda para resolver su misterio pero tengan mucho cuidado el contenido del audio podria causarte traumas psicologicos, hasta la propia muerte!..suerte..\r\n\r\n<a href=\\\'./Crypt/voz_diabolico.wav\\\'>Descargar/Abrir</a>','Conviertete en un detective CSI.y resuelve el contenido de la cinta diabolica..suerte..XD..!!!','kyr4-bl4ck',8,'yes','','69f72ae5d3102c373d7eea6348edbd59',0),(29,'Texto Oculto','non','Bienvenido Agente!!Durante la Batalla de Waterloo, napoleon mando un mensaje cifrado al general Kleber,tu mision sera decifrar el dichiso mensaje..!! suerte..\r\n\r\n<a href=\\\'./Crypt/mensajeoculto.png\\\'>Descargar/Abrir</a>\r\n\r\n\r\n','Bienvenido Agente Acontinuacion se presenta un mensaje oculto tu misi&oacute;n sera decifrarlo..!!! suerte..XD..!!','kyr4-bl4ck',8,'yes','','8d63d78ef2e708e033707409be380dec',0);
/*!40000 ALTER TABLE `tb_warghxc` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_wxuhxc`
--

DROP TABLE IF EXISTS `tb_wxuhxc`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_wxuhxc` (
  `_idWxu` int(11) NOT NULL AUTO_INCREMENT,
  `_idWar` int(11) NOT NULL,
  `_nickUsu` varchar(30) NOT NULL,
  PRIMARY KEY (`_idWxu`),
  KEY `_idWar` (`_idWar`,`_nickUsu`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;